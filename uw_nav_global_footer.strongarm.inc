<?php

/**
 * @file
 * uw_nav_global_footer.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_nav_global_footer_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'xmlsitemap_settings_menu_link_menu-uw-menu-global-header';
  $strongarm->value = array(
    'status' => '0',
    'priority' => '0.5',
  );
  $export['xmlsitemap_settings_menu_link_menu-uw-menu-global-header'] = $strongarm;

  return $export;
}

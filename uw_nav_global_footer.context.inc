<?php

/**
 * @file
 * uw_nav_global_footer.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_nav_global_footer_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'global_footer';
  $context->description = 'Menu placement and static links';
  $context->tag = 'Navigation';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_nav_global_footer-footer-1' => array(
          'module' => 'uw_nav_global_footer',
          'delta' => 'footer-1',
          'region' => 'global_footer',
          'weight' => '-12',
        ),
        'uw_nav_global_footer-footer-2' => array(
          'module' => 'uw_nav_global_footer',
          'delta' => 'footer-2',
          'region' => 'global_footer',
          'weight' => '-11',
        ),
        'uw_nav_global_footer-footer-3' => array(
          'module' => 'uw_nav_global_footer',
          'delta' => 'footer-3',
          'region' => 'global_footer',
          'weight' => '-10',
        ),
        'uw_nav_global_footer-social' => array(
          'module' => 'uw_nav_global_footer',
          'delta' => 'social',
          'region' => 'global_footer',
          'weight' => '-9',
        ),
        'uw_auth_cas_common-cas' => array(
          'module' => 'uw_auth_cas_common',
          'delta' => 'cas',
          'region' => 'global_footer',
          'weight' => -8,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Menu placement and static links');
  t('Navigation');
  $export['global_footer'] = $context;

  return $export;
}

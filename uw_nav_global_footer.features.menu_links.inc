<?php

/**
 * @file
 * uw_nav_global_footer.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_nav_global_footer_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-uw-menu-global-header_about-waterloo:https://uwaterloo.ca/about
  $menu_links['menu-uw-menu-global-header_about-waterloo:https://uwaterloo.ca/about'] = array(
    'menu_name' => 'menu-uw-menu-global-header',
    'link_path' => 'https://uwaterloo.ca/about',
    'router_path' => '',
    'link_title' => 'About Waterloo',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-uw-menu-global-header_about-waterloo:https://uwaterloo.ca/about',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'en',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-uw-menu-global-header_admissions:https://uwaterloo.ca/admissions
  $menu_links['menu-uw-menu-global-header_admissions:https://uwaterloo.ca/admissions'] = array(
    'menu_name' => 'menu-uw-menu-global-header',
    'link_path' => 'https://uwaterloo.ca/admissions',
    'router_path' => '',
    'link_title' => 'Admissions',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-uw-menu-global-header_admissions:https://uwaterloo.ca/admissions',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-uw-menu-global-header_faculties--academics:https://uwaterloo.ca/faculties-academics
  $menu_links['menu-uw-menu-global-header_faculties--academics:https://uwaterloo.ca/faculties-academics'] = array(
    'menu_name' => 'menu-uw-menu-global-header',
    'link_path' => 'https://uwaterloo.ca/faculties-academics',
    'router_path' => '',
    'link_title' => 'Faculties & academics',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-uw-menu-global-header_faculties--academics:https://uwaterloo.ca/faculties-academics',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'en',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-uw-menu-global-header_offices--services:https://uwaterloo.ca/offices-services
  $menu_links['menu-uw-menu-global-header_offices--services:https://uwaterloo.ca/offices-services'] = array(
    'menu_name' => 'menu-uw-menu-global-header',
    'link_path' => 'https://uwaterloo.ca/offices-services',
    'router_path' => '',
    'link_title' => 'Offices & services',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-uw-menu-global-header_offices--services:https://uwaterloo.ca/offices-services',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'en',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-uw-menu-global-header_support-waterloo:https://uwaterloo.ca/support
  $menu_links['menu-uw-menu-global-header_support-waterloo:https://uwaterloo.ca/support'] = array(
    'menu_name' => 'menu-uw-menu-global-header',
    'link_path' => 'https://uwaterloo.ca/support',
    'router_path' => '',
    'link_title' => 'Support Waterloo',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-uw-menu-global-header_support-waterloo:https://uwaterloo.ca/support',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'language' => 'en',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-uw-menu-global-header_covid-19:https://uwaterloo.ca/coronavirus
  $menu_links['menu-uw-menu-global-header_support-waterloo:https://uwaterloo.ca/coronavirus'] = array(
    'menu_name' => 'menu-uw-menu-global-header',
    'link_path' => 'https://uwaterloo.ca/coronavirus',
    'router_path' => '',
    'link_title' => 'COVID-19',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
        'identifier' => 'menu-uw-menu-global-header_covid-19:https://uwaterloo.ca/coronavirus',
      ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'language' => 'en',
    'menu_links_customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About Waterloo');
  t('Admissions');
  t('Faculties & academics');
  t('Offices & services');
  t('Support Waterloo');
  t('COVID-19');

  return $menu_links;
}

<?php

/**
 * @file
 * uw_nav_global_footer.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function uw_nav_global_footer_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-uw-menu-global-header.
  $menus['menu-uw-menu-global-header'] = array(
    'menu_name' => 'menu-uw-menu-global-header',
    'title' => 'Global Header',
    'description' => 'The <em>Global Header</em> menu is used on all sites to display University-owned links as defined by MSC.',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Global Header');
  t('The <em>Global Header</em> menu is used on all sites to display University-owned links as defined by MSC.');

  return $menus;
}

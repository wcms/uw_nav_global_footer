<?php

/**
 * @file
 * uw_nav_global_footer.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_nav_global_footer_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer menu-uw-menu-global-header menu items'.
  $permissions['administer menu-uw-menu-global-header menu items'] = array(
    'name' => 'administer menu-uw-menu-global-header menu items',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'menu_admin_per_menu',
  );

  return $permissions;
}
